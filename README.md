# Vegoans Community Domains Setup

This repository stores the current domain names setup used by the Vegoans Community.
If you want to add a subdomain or add a new domain name related with Vegoans Community or 
any other change, please read the Contributing section.

## Contributing

Please consider reading the [contributing guide](CONTRIBUTING.md) if you want to contribute to the project or create new content.

## Code of Conduct

The community is one of the best features of the Vegoans Community, and we want to ensure it remains welcoming and safe for everyone.
We have adopted the Contributor Covenant for all projects in the @Vegoans Gitlab group, the discussion forum, chat rooms, mailing list, social media tools, meetups and any other public event related to Vegoa.
This code of conduct outlines the expectations for all community members, as well as steps to report unacceptable behavior.
We are committed to providing a welcoming and inspiring community for all and expect our code of conduct to be honored.

* **The Code of Conduct is available [here](CODE_OF_CONDUCT.md).**
